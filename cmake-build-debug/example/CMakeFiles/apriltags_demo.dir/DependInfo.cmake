# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/advaithsai/CLionProjects/apriltags/example/Serial.cpp" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/example/CMakeFiles/apriltags_demo.dir/Serial.cpp.o"
  "/Users/advaithsai/CLionProjects/apriltags/example/apriltags_demo.cpp" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/example/CMakeFiles/apriltags_demo.dir/apriltags_demo.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "include"
  "/usr/local/include"
  "../AprilTags"
  "../."
  "/opt/local/include/eigen3"
  "/opt/local/include"
  "/opt/local/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
