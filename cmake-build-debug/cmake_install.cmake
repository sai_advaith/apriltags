# Install script for directory: /Users/advaithsai/CLionProjects/apriltags

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/lib/libapriltags.a")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libapriltags.a" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libapriltags.a")
    execute_process(COMMAND "/Library/Developer/CommandLineTools/usr/bin/ranlib" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libapriltags.a")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/AprilTags" TYPE FILE FILES
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/Edge.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/FloatImage.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/GLine2D.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/GLineSegment2D.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/Gaussian.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/GrayModel.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/Gridder.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/Homography33.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/MathUtil.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/Quad.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/Segment.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/Tag16h5.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/Tag16h5_other.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/Tag25h7.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/Tag25h9.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/Tag36h11.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/Tag36h11_other.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/Tag36h9.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/TagDetection.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/TagDetector.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/TagFamily.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/UnionFindSimple.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/XYWeight.h"
    "/Users/advaithsai/CLionProjects/apriltags/AprilTags/pch.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/lib/pkgconfig/apriltags.pc")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/example/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
