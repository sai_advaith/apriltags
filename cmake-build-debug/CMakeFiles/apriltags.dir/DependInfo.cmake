# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/advaithsai/CLionProjects/apriltags/src/Edge.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/Edge.cc.o"
  "/Users/advaithsai/CLionProjects/apriltags/src/FloatImage.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/FloatImage.cc.o"
  "/Users/advaithsai/CLionProjects/apriltags/src/GLine2D.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/GLine2D.cc.o"
  "/Users/advaithsai/CLionProjects/apriltags/src/GLineSegment2D.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/GLineSegment2D.cc.o"
  "/Users/advaithsai/CLionProjects/apriltags/src/Gaussian.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/Gaussian.cc.o"
  "/Users/advaithsai/CLionProjects/apriltags/src/GrayModel.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/GrayModel.cc.o"
  "/Users/advaithsai/CLionProjects/apriltags/src/Homography33.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/Homography33.cc.o"
  "/Users/advaithsai/CLionProjects/apriltags/src/MathUtil.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/MathUtil.cc.o"
  "/Users/advaithsai/CLionProjects/apriltags/src/Quad.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/Quad.cc.o"
  "/Users/advaithsai/CLionProjects/apriltags/src/Segment.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/Segment.cc.o"
  "/Users/advaithsai/CLionProjects/apriltags/src/TagDetection.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/TagDetection.cc.o"
  "/Users/advaithsai/CLionProjects/apriltags/src/TagDetector.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/TagDetector.cc.o"
  "/Users/advaithsai/CLionProjects/apriltags/src/TagFamily.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/TagFamily.cc.o"
  "/Users/advaithsai/CLionProjects/apriltags/src/UnionFindSimple.cc" "/Users/advaithsai/CLionProjects/apriltags/cmake-build-debug/CMakeFiles/apriltags.dir/src/UnionFindSimple.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "include"
  "/usr/local/include"
  "../AprilTags"
  "../."
  "/opt/local/include/eigen3"
  "/opt/local/include"
  "/opt/local/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
